# Debconf translations for iog.
# Copyright (C) 2012 THE iog'S COPYRIGHT HOLDER
# This file is distributed under the same license as the iog package.
# Adriano Rafael Gomes <adrianorg@gmail.com>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: iog 1.03-3.6\n"
"Report-Msgid-Bugs-To: iog@packages.debian.org\n"
"POT-Creation-Date: 2010-02-25 07:42+0100\n"
"PO-Revision-Date: 2012-08-27 13:05-0300\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@gmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../iog.templates:2001
msgid "Migrate old install out of /var/www/iog/?"
msgstr "Migrar a instalação antiga para fora de /var/www/iog/?"

#. Type: boolean
#. Description
#: ../iog.templates:2001
msgid ""
"A previous package release has left data installed in the /var/www/iog/ "
"directory. Current versions of the IOG package now use /var/lib/iog/ for IOG "
"data files."
msgstr ""
"Uma versão anterior do pacote deixou dados instalados no diretório /var/www/"
"iog/. As versões atuais do pacote IOG usam agora /var/lib/iog/ para os "
"arquivos de dados."

#. Type: boolean
#. Description
#: ../iog.templates:2001
msgid ""
"If you choose this option, all existing network data will be moved to the "
"new location."
msgstr ""
"Se você escolher essa opção, todos os dados de rede existentes serão movidos "
"para o novo local."

#. Type: boolean
#. Description
#: ../iog.templates:2001
msgid ""
"Consequently, directory settings in the IOG configuration file (/etc/iog."
"cfg) will be changed to /var/lib/iog and a web server alias declaration will "
"be added so that old network statistics are still published."
msgstr ""
"Consequentemente, as configurações de diretórios no arquivo de configuração "
"do IOG (/etc/iog.cfg) serão alteradas para /var/lib/iog e será adicionada "
"uma declaração de alias para o servidor web, de forma que as estatísticas de "
"rede antigas continuem publicadas."

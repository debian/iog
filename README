// IOG v1.03 - Input/Output Grapher - README
//
// Official Homepage: http://www.dynw.com/iog/
// Comments, bug reports to: iog@dynw.com
//
// Copyright (c) 2000-2003 James Dogopoulos <jd@dynw.com>
//
// Contributors:
// Simon Leinen <simon@switch.ch> - SNMPlib
//

Files:

Artistic	- Artistic License *READ*
*.gif		- IOG image files.
iog		- IOG execution file.
iog.cfg		- IOG Configuration file.
README		- THIS FILE
BER.pm		- Module by Simon Leinen <simon@switch.ch>
SNMP_Session.pm	- Module by Simon Leinen <simon@switch.ch>
howto-iog.html	- How-to by Sean Kelly <sean@inabiaf.com>
		  http://www.inabiaf.com/howto-iog/
index.php	- Index script by Preston Carter <preston@ashenet.net>

About:

IOG is a network I/O byte grapher made to graph cumulative KB/MB/GB
totals for hours/days and months. It is intended to be simple, fast
(support thousands of hosts) and integrate well with MRTG. Data for
each host is updated hourly and HTML graphs are created. It uses a
data consolidation algorithm which allows for a small, non-growing
database file for each host. No external graphing libs or
executables are required.

IOG has been used in several production ISP environments,
including at the authors company, Dynamic Internet (dyni.net).

MRTG: http://ee-staff.ethz.ch/~oetiker/webtools/mrtg/mrtg.html


Comments:

64-bit SNMPv2c counters can now be used, use ifHCInOctets.# and
ifHCOutOctets.# in iog.cfg.

NT support has been added (tested under win2k), to enable full NT
support pass the command line argument "NT" to iog.
	ex.) "perl iog NT"
You will need to add entries to "at" to make it run once per hour.

Thorough performance testing has not been done, but IOG should
be able to handle >1000 hosts/ports adequately. If you are running
on a semi-large network with at least 200 ports, please report your
results to iog@dynw.com.

*IMPORTANT*: You must select the proper sysUpTime value for your
device otherwise problems will arrive when these devices reboot.


Installation:

Required...

* WindowsNT/2000/XP with Perl/ActivePerl or your favorite UNIX OS
  with Perl.

* A web browser (if you intend to let people view these pages through the
  world wide web).

* A switch/router/device with an SNMPd/server configured.


1.) Untar/gz into your desired location. copy all .gif files to any
    directory that will contain HTML (path in .cfg).

2.) Edit "iog.cfg" and follow the examples to set-up your hosts.

3.) Add a crontab to run "iog" EVERY HOUR (NOT every 5 minutes).
        0 * * * * /path/to/iog
    ..or an "at" entry on NT.

4.) Decide how you want your users to view the stats. Perhaps
    you want to add a type of authentication to view the graphs (.htaccess)?

5.) Watch it run and report bugs and performance info to iog@dynw.com.
    Try to include detailed info including OS type, Perl version etc.



Upgrading:

From 1.0rcX - When upgrading, the first hour might be in-accurate due to a
minor change in db storage code.

From v0.99 - Make a copy of your iog.cfg and copy over all the files in this
directory.

From v0.98 or earlier - The format of the iog.cfg file has changed,
please see iog.cfg!



Donations:

No need for donations, but if you feel like doing something generous, send
some business to the author's company at www.dyni.net or you can donate
to the creator of SNMPlib or MRTG, they also produce great free software.



Changes:

v1.03		* Fixed bug which causes router/switch reboots to potentially
		  cause large negative values for that particular hour.

v1.02		* Added newer SNMP module that fixes compatibility with
		  some switches. Included how-to by Sean Kelly.

v1.01		* Fixed small "NE" bug by changing to lower-case.

v1.00           * Previous months and days are now saved and linked to
                  provide a more historical view of usage.
                * Added 64-bit SNMPv2c counter support, backwards
                  compatible with SNMPv1.
                * Added easier way to change KB/MB/GB size to increase
                  accuracy.

v0.99		* Added uptime polling to detect hosts that have
		  reset and adjust counters accordingly.
		  (NOTE: Config file format has changed.)
		* Fixed bug that caused iog to die when a hostname
		  could not be resolved.
		* Fixed reporting bug for hosts that are unreachable.

v0.98		* Fixed bug causing January to not graph properly.
		* Changed "This Year" text to "Previous Months".

v0.97		* Fixed interface counter wrap bug, previous fix was
		  not complete.

v0.96		* Fixed problem with in-accurate reporting when an
		  interface counter wraps (every 4GB).
		  File name is now displayed on front of HTML.

v0.95		* Failed SNMPGets no longer reset counter to zero.

v0.9            * 0KB cells are no longer displayed.
		* Problem with top navigation during hour 1:00 has been
		  corrected.
		* "months" graph now displays GB total for In+Out.

v0.8		* Fixed bug in monthly graph creation, totals were
		  not being calculated properly.
		* Minor changes in MB/GB display.

v0.7		* Preliminary NT support added.
		* MB(KB) now scales to GB(MB) when >1000mb to
		  better display stats for busy hosts/ports.

v0.6		* Fixed 0kb bars under Netscape.
		* Now detects when a hosts octet counters have reset to
		  prevent negative totals from being displayed.
		* Navbar now only displays reports which are available.
		* HTML is now generated at first run, to avoid confusion.
		* Improved documentation.

v0.5            * First public release. (10/12/2000)
v0.4		*
v0.1 - v0.3     * Internal development releases.


